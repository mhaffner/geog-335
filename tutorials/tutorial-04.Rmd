---
title: "GEOG-335: Geographic Information Systems I"
author: Tutorial 4
date: "Due: Friday, 04 October 2019"
output:
  prettydoc::html_pretty:
    theme: hpstr 
    highlight: github
---

# Instructions

Complete the tutorial in Chapter 4 from p. 123 to p. 137 (use the numbers at the
bottom of the page). Then, complete the exercises on p. 138. It is a good idea
to reflect on the answers within the tutorials, but these will not be turned in.
Similarly, you will not need to turn in chapter review questions. **You will
only turn in the exercises on p. 138.** You do not need to complete the
Challenge Problem. Upload your submission to the dropbox on Canvas as a .pdf.

# Tutorial notes

- The little pictures next to the instructions are very helpful. Use them!
- If you have problems on **Step 21** (even after trying the author's
  suggestions), use `POP00_SQMI` with no normalization instead.
- On **Step 24**, look for the "label" heading within the "Symbology" tab.
- When creating maps, it can be useful to copy a layer, try different
  classification schemes and colors, and hide/unhide the layer to compare the
  effects against the original.
- The statement on p. 132 is problematic: "An advantage of unclassed maps, such
  as proportional symbols maps is that they don't require classification and
  present an unbiased view of the data." Bias can never be removed from maps!

# Exercise question notes

- If an exercise question says to simply do something, explain your methodolody
  (e.g., tools used, order of steps, etc.), and provide a screenshot of the
  dialog box or map (whichever is more relevant) once it is complete.
