---
title: "GEOG-335: Geographic Information Systems I"
author: Tutorial 3
date: "Due: Friday, 27 September 2019"
output:
  prettydoc::html_pretty:
    theme: hpstr 
    highlight: github
---

# Instructions

Complete the tutorial in Chapter 3 from p. 89 to p. 105 (use the numbers at the
bottom of the page). Then, complete the exercises on p. 106. It is a good idea to
reflect on the answers within the tutorials, but these will not be turned in.
Similarly, you will not need to turn in chapter review questions. **You will
only turn in the exercises on p. 106.** You do not need to complete the Challenge
Problem. Upload your submission to the dropbox on Canvas as a .pdf.

# Tutorial notes

- This tutorial is a bit long so it may be a good idea to work on it some
  outside of class on Wednesday.
- Start to develop a habit of searching for coordinate systems rather than
  navigating through series of dropdown lists. Also, save commonly used systems
  as favorites.
- The tip on page 93 (after **Step 17**) is crucially important.
- On **Step 25**, NAD 1983 can be found under "USA and territories" rather than
  "North America".
- Due to things like on-the-fly projection, it's sometimes difficult to tell if
  ArcMap is modifying the underlying data or only the appearance.
- On **Step 46**, be sure to type in the web address fully exactly how it's
  written in the book. if you can't download the image easily, right click
  within the window and click "view page source." You can usually find the image
  by digging around in the html.

# Exercise question notes

- If an exercise question says to simply do something, explain your methodolody
  (e.g., tools used, order of steps, etc.), and provide a screenshot of the
  dialog box or map (whichever is more relevant) once it is complete.
