---
title: "GEOG-335: Geographic Information Systems I"
author: Tutorial 8
date: "Due: Friday, 15 November 2019"
output:
  prettydoc::html_pretty:
    theme: hpstr 
    highlight: github
---

# Instructions

Complete the tutorial in Chapter 8 from p. 245 to p. 257 (use the numbers at the
bottom of the page). Then, complete the exercises on p. 258. It is a good idea
to reflect on the answers within the tutorials, but these will not be turned in.
Similarly, you will not need to turn in chapter review questions. **You will
only turn in the exercises on p. 258.** You do not need to complete the
Challenge Problem. Upload your submission to the dropbox on Canvas as a .pdf.

# Tutorial notes

- The workflow suggested in **Step 17** is not easily reproducible. If you find
  yourself completing operations like this often, it's time to start scripting!
- Keep in mind that many tool names that perform similar options have different
  names for vector vs. raster data.

# Exercise question notes

- TBA
