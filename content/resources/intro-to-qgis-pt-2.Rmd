---
title: "Introduction to QGIS (pt. 2)"
draft: false
---

## Attribute queries

1. Use the data source manager to add the "counties2" geopackage to the map.

2. Right click on the counties dataset and select "filter".

3. Create a query which selects counties where the `POP2010` field is greater
   than 100,000. How many counties are returned by the query?

4. Next, create a query where the `POP2010` field is greater than 100,000 and
   where `POP2014` is less than `POP2014`. How many counties are returned by the
   query? 

5. How would you describe what this query captures? How would you describe the
   spatial pattern of these counties?

6. Finally, create a query which fulfills all three of the following criteria:

- `POP2010` is less than 40000
- `MED_AGE` is between 35 and 45
- `STATE_NAME` is equal to California, New York, or Florida

## Basic geoprocessing

1. Add a US counties dataset to the map.

2. Transform the underlying spatial data into EPSG:2163

3. Create a buffer around the counties.
