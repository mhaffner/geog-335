---
title: "Activity: Introduction to QGIS (pt. 1)"
draft: false
---

## Adding data to the map

1. Use the data source manager to add the "counties" geopackage to the map: 
`Q:\StudentCoursework\Haffnerm\Data\geog_335\usa_counties.gpkg`

2. Use the Browser to add an additional data source to the map.

3. Add three other data sources to the map using a combination a methods. Be
   sure at least one is a raster dataset and another is a shapefile.

## Managing and installing plugins

1. From the top menu, go to "Plugins > Manage and Install Plugins".

2. Search for QuickMapServices and install it.

3. Add a basemap to the map.

4. Open the "Search QMS" window.

5. Search for "Stamen" and add a few Stamen basemaps.

6. Search for other basemaps and add a few to the map (other good search options
   would be "Carto" and "Mapbox").

4. Find another plugin and install it.
