import arcpy

arcpy.env.workspace = "Q:/StudentCoursework/Haffnerm/GEOG.335.002.2201/LAB/data/shp"

# get list of feature classes
in_shps = arcpy.ListFeatureClasses()

# declare a variable for the geodatabase
out_gdb = "Q:/StudentCoursework/Haffnerm/GEOG.335.002.2201/HAFFNERM/data/tests.gdb"

# convert
arcpy.FeatureClassToGeodatabase_conversion(in_shps, out_gdb)
