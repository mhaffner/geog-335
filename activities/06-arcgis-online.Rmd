---
title: ArcGIS Online Demo
date: Thursday, October 10, 2019
output: 
  prettydoc::html_pretty:
    theme: hpstr 
    highlight: github
---

# Setup

In this activity you will gain some experience using ArcGIS Online. To login:

1. Navigate to https://www.arcgis.com/.
2. Click "Sign In".
3. Select Enterprise Login.
4. Enter "uwec" in the textbox so that the URL reads "uwec.maps.arcgis.com".
5. Enter your UWEC credentials.
6. Celebrate.


# Activity

In this map document, we will add data in a few different ways. Click the "Add"
button to see the options available. First we will add data from a local file.

## Local file

1. Find a file on your local machine (a zipped shapefile or a .geojson file).
2. Click the "Add" button.
3. Next, click the "Add layer from file" button.
4. Navigate to the location of the file on your local machine and select it. It
   may take a minute for the data to appear, and you need to deselect/select the
   layer and/or zoom in/out to get it to appear.

## Web source

Next, we will add a layer from a web source.

1. Navigate to "data.gov" and search "kml".
2. Click on the first link that appears. It should be "Schools - KML".
3. Right click on the "Download" link. Select "Copy link location".
4. From ArcGIS Online, click the "Add" button. 
5. This time, click the "Add layer from web" button.
6. Paste the link in, then click ok.

## Web map service

Next, we will add another layer from a web source but using a web map service
(WMS) rather than a flat file.

1. Navigate to the MN IT Services page here:
   http://www.mngeo.state.mn.us/chouse/wms/geo_image_server.html.
2. Click on the "List of photos, hillshade, and topographic maps" link to view
   some of the available layers.
3. Go back to the previous page. 
4. Next, click on "How to use the service in ArcGIS Online"
5. After you understand the instructions here, add a service of your choice to
   the ArcGIS Online map.
6. What is the error message received and why does it occur?
   
## Save and publish 
   
Next, we will save and publish the map.

1. Click "Save" and select an appropriate name.
2. Next, click "Share" and enter the required information.
3. Check the box that reads "Everyone". Open the link in a different browser and
   view the resulting map.

