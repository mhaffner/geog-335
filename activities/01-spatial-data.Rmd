---
title: "GEOG-335: Geographic Information Systems I"
author: In-class activity 1
date: Getting to know spatial data
output:
  prettydoc::html_pretty:
    theme: hpstr 
    highlight: github
---

# Activity

In this activity you will work with a few different spatial data formats. Though
shapefiles are not as commonly used today as other formats, they have important
historical significance and make for a useful exercise in understanding how
spatial data are stored.

`r i<-1; i`. Download and unzip the [`shp_ex.zip`](../data/shp_ex.zip). Here,
there should be a number of files. View the contents with a file explorer. How
many do you see?

```{r echo = FALSE, eval = FALSE, message = FALSE, warning = FALSE}
# there should be 5 files here
```

`r i<-i+1; i`. Open a GIS that has a file browser (e.g., the QGIS browser or
ArcCatalog). Now how many files do you see? What is the reason for this
difference?

```{r echo = FALSE, eval = FALSE, message = FALSE, warning = FALSE}
# there should be x spatial data sets here; these actually are not files. The
# reason for the difference is that GIS programs hide the "unimportant" files.
```

`r i<-i+1; i`. Go back to your file browser. We will try to open the following
files in a text editor. Before each, predict what you see. Then attempt to
explain what each is, and fill in the following chart:

<div>
| Extension | Description |
|-----------|-------------|
| .cpg      |             |
| .shp      |             |
| .shx      |             |
| .dbf      |             |
| .prj      |             |
</div>

```{r echo = FALSE, eval = FALSE, message = FALSE, warning = FALSE}
# they won't be able to see the .shx and .shp in a text editor. The .dbf might
# look weird too, but they should be able to open the .dbf in Excel
```
 
`r i<-i+1; i`. Now, download the `geojson_ex.geojson`. Open this file in a text
editor. What do you see?

```{r echo = FALSE, eval = FALSE, message = FALSE, warning = FALSE}
# should see everything in plain text, but organized in a different way (not tabular).
```

`r i<-i+1; i`. Now let's open the shapefile in a desktop GIS. What do you see?

```{r echo = FALSE, eval = FALSE, message = FALSE, warning = FALSE}
# Should be able to see Wisconsin counties
```

Next we will fill in another column in the table and answer a few more
questions.

`r i<-i+1; i`. Remove the "shp_ex" shapefile layer from the map. Go to the
Windows file browser, and "hide" the `.cpg` file by putting it into a new
directory called `safe` (so that it can be retrieved later). Before adding the
layer back to the map document, predict what will happen.

`r i<-i+1; i`. Add the layer back to the map document. What happened? Did this
align with your expectations?

`r i<-i+1; i`. Remove the "shp_ex" layer from the map again. Put `.cpg` file
back into the directory with the other components of the shapefile. Go to the
Windows file browser, and "hide" the `.shp` file by putting it into the `safe`
directory (so that it can be retrieved later). Before adding the layer back to
the map document, predict what will happen.

`r i<-i+1; i`. Repeat this for the remaining three files, and complete the
table below.

<div>
| Extension | Description | Required to display spatial data? |
|-----------|-------------|-----------------------------------|
| .cpg      |             |                                   |
| .shp      |             |                                   |
| .shx      |             |                                   |
| .dbf      |             |                                   |
| .prj      |             |                                   |
</div>
 
`r i<-i+1; i`. Next, add the `.geojson` file to the map. Does the data look any
different on the map document?

`r i<-i+1; i`. Make a list of other vector spatial data formats below.
