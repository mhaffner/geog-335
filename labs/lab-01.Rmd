---
title: "GEOG-335: Geographic Information Systems I"
author: "Lab I: Coordinate Reference Systems"
date: "Due: Friday, 18 October 2019"
output:
  prettydoc::html_pretty:
    theme: architect 
    highlight: github
---

# Overview

The main goal this lab exercise is to (a) evaluate your understanding of
geographic and projected coordinate systems (b) and apply these concepts to GIS
data.

## Objectives

- Build several data frames displaying feature data of the world in different
  coordinate systems.
- Reproject US States data, and define a projection for Wisconsin state data.
- Work with datasets of different coordinate systems.
- Create a map displaying data in several different coordinate systems.
- Create a map of NW Wisconsin counties and streams.

## Data Management

Download the [lab-01-data.zip](../data/lab-01-data.zip) file and put it in
your folder on the `Q:\` drive. Unzip it in your `data\` folder

## Data sources

- Road features come from the *Michigan Department of Transportation*.
- Central Wisconsin data and all others come from the `mgisdata` that
  comes with the Price book.

## Instructions and Setup

- Type your answers to questions in a Word document or another similar format.
Include all maps as properly cropped images. There are samples of how maps
should be formatted at the end of this lab created by your peers in previous semesters.
- Put the lab data in a dedicated folder for Lab 1.
- Set up the environments:
   - Go to *Geoprocessing -> Environments*
   - In the workspace settings, set your current and scratch workspace
     to your Lab 1 folder. This will set the default location for all
     new files. It is a good idea to do this each time you undertake a
     project in ArcMap.
   - From the File menu, click on **Map Document Properties**. Toward
     the bottom, click the box that reads **Store relative pathnames to
     data sources**

## Final Product 

Upload a `.pdf` to the Dropbox on Canvas with (a) answers to the questions,
(b) an image containing six world maps and a map of Wisconsin, (c) and an image
with a single map of counties near Eau Claire and streams in this area.

## Notes

- Refer to the Price book (particularly Chapter 3) for help. This lab
  covers material similar to the tutorials but provides less
  instruction.
- Use the ArcGIS Desktop Help to search for /geographic coordinate
  systems/ to better understand what these are.

## Legend

- `File_names` (such as shapefiles) appear as code formatted.
- `Folder_names` appear as code formatted.
- `File extensions` appear as code formatted.
- **Dialog boxes** are bold faced.
- **Tabs** are bold faced.
- *Folders* are italicized.
- *Layers* are italicized.
- *Checkbox items* are italicized.
- *Radio button items* are italicized.
- *Text box names* are italicized.
- *Nested -> menu items -> with series -> of steps -> or clicks* are italicized with arrows.
- "Menu items" are quoted.
- "Data frame names" are quoted.
- "Coordinate system names" are quoted.
- "Button names" are quoted.

Some exceptions may apply, and some mistakes may be present. If anything seems
ambiguous, go with the instructions over how the item is styled (italicized,
quoted, etc.).

# Part I: Set World Predefined Coordinate Reference Systems in Data Frames

`r i<-1; i`. What is a relative path? Describe it in your own words.

## Section 1: Add layers to the data frame

From the `WORLD` subfolder in the `lab-01-data` folder, add the shapefiles
`country` and `geogrid` to ArcMap. If you receive an error message, just click
OK. Be sure to order layers so that the *geogrid* layer is underneath the
*country* layer. Zoom to full extent.

- Change the layer symbolization to something aesthetically pleasing for both
  shapefiles.
- *Organize the first data frame and choose a coordinate system*: Right-click on
  the data frame (currently titled "Layers"). Open the **Properties** dialog
  box, and under the **General** tab, change the name of this data frame to
  "Geographic Coordinate System". Under the **Coordinate System** tab, select
  *WGS 1984* which can be found under *Geographic Coordinate Systems -> World ->
  WGS 1984*. This data frame is now complete.

- *Save your map*: Under the "File" menu, select "Save As", and save your map
  document (as a `.mxd` file) in your Lab 1 folder. Remember to periodically
  save your work by clicking "Save" (or use the keyboard shortcut *Ctrl + s*).

## Section 2: Build additional data frames

- Add a new data frame to your map. Within the new data frame, add
  both the `country` and `geogrid` shapefiles as you did previously.
  Make sure that the *geogrid* layer is underneath the *country* layer
  again, and change the layer symbolization to your liking. Change the
  name of this data frame to "Mercator Projection".
- *Change the data frame coordinate system*: Open the data frame's
  **Properties** dialog box and select the **Coordinate System** tab. Choose the
  "Mercator (world)" projection from *Projected coordinate systems -> World ->
  Mercator (world)*. Click "OK". If you encounter an error message again, just
  click "Yes".
- *Create additional data frames*: Repeat the steps above to create three more
  data frames. Two coordinate systems are provided below. For the last data
  frame, choose a different coordinate system of your choice.
  a. "Sinusoidal" from *Projected Coordinate Systems -> World ->
     Sinusoidal*
  b. "Equidistant-Conic" from *Projected Coordinate Systems -> World
     -> Equidistant Conic*
  c. Your choice

# Part II: State Level Coordinate Systems 
## Section 1: Create a data frame for Wisconsin data

- Add a new data frame to the map. Name this data frame "Wisconsin UTM". Add the
  `states` shapefile from the `USA` folder to the map document.
- *Create a shapefile for Wisconsin*: Using the "Select Features" tool, select
  Wisconsin from the *states* layer. Right click on the layer and click
  *Selection -> Create layer from selected features*. Right click on the newly
  created *states selection* layer, and click *Data -> Export Data*. Export the
  selected features **as a shapefile, not a geodatabase feature class** using
  the same coordinate system as the current source data layer. Be sure to modify
  the output file location to your Lab 1 folder.
- Click "Yes" to add this new data source to the data frame. Remove the *states*
  and *states selection* layers from the data frame. Zoom to full extent.
- *Change the data frame coordinate system*: Using the previous instructions,
  set the data frame coordinate system to "NAD 1983 UTM Zone 16 N" which can be
  found in _Projected Coordinate Systems -> UTM -> NAD 1983 -> NAD 1983 UTM Zone
  16 N_.

## Section 2: Explore CRS information
- From ArcCatalog, navigate to your Lab 1 data folder. Expand the
  folder and click on the `states` shapefile.
 
`r i<-i+1;i`. What is the coordinate reference system of `states`?

- From ArcCatalog, navigate to your Lab 1 data folder. Expand the
  folder and click on the `stroads_miv5a` shapefile.

`r i<-i+1;i`. What is the coordinate system of `stroads_miv5a`?

- Insert a new data frame and name it "States". Add the `states`
  shapefile from your folder to this data frame. Add the
  `stroads_miv5a` to this data frame also.

`r i<-i+1;i`. Do the two shapefiles appear to be aligned? If so, why would
shapefiles with different coordinate systems align?

- *Change the projection of the roads dataset*: Project the `stroads_miv5a`
  shapefile to match the CRS of the `states` shapefile. Use the tool from *Data
  management -> Projections and Transformations -> Feature -> Project*. **Do not
  use "Define Projection".**

`r i<-i+1;i`. What is the difference between the "Project" and the "Define Projection" tools?

- Open the "States" data frame properties window. Go to the **Coordinate
  System** tab. Change the CRS to *Projected coordinate systems -> Continental
  -> North American Lambert Conformal Conic*. Click "Apply" then "OK".
- Zoom to full extent.
- Open the layer properties for the *states* layer. Click on the
  **Source** tab. Note the coordinate system listed.

`r i<-i+1;i`. Is this coordinate system different from what is listed in the
data frame properties?

`r i<-i+1;i`. What can you conclude about the data frame's coordinate system
and a shapefile's coordinate system?
 
# Part III: Create a Map

- *Map creation*: Switch to layout view in ArcMap. Move and resize all
  7 data frames so that you can see each on the map layout.
- *Labeling and map styling*: Using the "Text" tool, label each data frame
  according to its title (i.e. coordinate system). Also include a global title
  for your map.
- Add other additional map components as you see fit. Be sure to create a
  cartographically pleasing map.
- *Map exporting*: From the "File" menu, click on "Export" to save
  your map as a `.jpg` image. 
- Add this map to your lab document that you will turn in.

# Part IV: Identify and Fix CRS Issues

In this part of the lab, you will project two features that have different CRS
problems. These features will then be overlaid to create a map. Feature 1 is a
shapefile of Northwest Wisconsin which includes Eau Claire and several
neighboring counties. The other shapefile contains the streams in these
counties.

- From the folder in *Central_Wisconsin*, add the shapefile
  `Central_WI_Cts.shp`. You will receive a warning a spatial
  reference, but click "OK".

`r i<-i+1;i`. What is this shapefile's coordinate system?

- *Adding appropriate coordinate system information*: Unfortunately, metadata is
  not attached to this shapefile, but assume the following information was given
  to you:

```
Metadata

GCS: North_America_1983
Datum: North_America_1983

```

`r i<-i+1;i`. Add the appropriate coordinate system information based on this
  newly acquired metadata.

`r i<-i+1;i`. What is the name of the coordinate system?

- Add the `Lower_Chip_strms` shapefile to the map.

`r i<-i+1;i`. Is there anything wrong with the appearance of this shapefile?

- Add appropriate coordinate system information to this shapefile if
  necessary.
- *Map creation*: Create a new map that shows the steams that flow through these
  counties. Export your final map as a `.jpg` and add it to your lab document.
- Upload your completed lab report to the Dropbox on Canvas.

# Appendix

The maps below were created by your peers in previous semesters. Your final maps
should look similar. However, do not feel obligated to use the exact same
colors, fonts, and arrangements. These maps are not perfect, but they are
certainly acceptable.

<center>
![](../img/lab-01/map-sample-01.jpg)

![](../img/lab-01/map-sample-02.jpg)
</center>
