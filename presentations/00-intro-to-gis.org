#+Title: GEOG-335

#+REVEAL_THEME: black
#+reveal_title_slide: nil
#+OPTIONS: reveal_width:1200 reveal_height:800 reveal_rolling_links:t
#+REVEAL_TRANS: none
#+REVEAL_HLEVEL: 2
#+REVEAL_MARGIN: 0.1
#+OPTIONS: num:nil toc:nil date:nil reveal_title_slide:nil
#+REVEAL_EXTRA_CSS: ../style/reveal/uwec.css
#+REVEAL_SLIDE_FOOTER: Intro to GIS
#+REVEAL_ROOT: ../reveal.js

* GEOG-335: Geographic Information Systems I                  
 :PROPERTIES:
 :reveal_background: ../img/detroit-filter.png
 :END:
Dr. Matthew Haffner \\ 
Department of Geography and Anthropology \\ 
University of Wisconsin - Eau Claire
* What is a GIS?
#+attr_reveal: :frag (appear)
- A computer-based system for storing, managing, analyzing, and
  mapping spatial data.
* Questions you could answer with a GIS 
#+REVEAL_HTML: <div class="column" style="float:right; width: 50%">
#+ATTR_HTML: :height 500px
[[../img/tornado-01.jpg]]
#+REVEAL_HTML: </div>
#+REVEAL_HTML: <div class="column no-write" style="float:left; width: 50%">
- Are the locations of tornadoes in Oklahoma clustered, dispered, or random?
#+REVEAL_HTML: </div>
* Questions you could answer with a GIS 
#+REVEAL_HTML: <div class="column" style="float:left; width: 60%">
#+ATTR_HTML: :height 500px
[[../img/vehicle-routing-01.png]]
#+REVEAL_HTML: </div>
#+REVEAL_HTML: <div class="column no-write" style="float:right; width: 40%">
- What is the optimal driving route for a group of delivery vehicles?
#+REVEAL_HTML: </div>
* Questions you could answer with a GIS 
#+REVEAL_HTML: <div class="column" style="float:left; width: 50%">
#+ATTR_HTML: :height 500px
[[../img/cabin-01.jpg]]
#+REVEAL_HTML: </div>
#+REVEAL_HTML: <div class="column no-write" style="float:right; width: 50%">
- What are the suitable locations for new construction of a cabin in
  northern Wisconsin?
#+REVEAL_HTML: </div>
* Major software platforms
#+ATTR_HTML: :height 500px
[[../img/esri-01.jpg]]
* Major software platforms
#+attr_reveal: :frag (appear)
- ESRI
  - ArcGIS ecosystem
    - ArcMap
    - ArcScene
    - ArcCatalog
  - ArcGIS Pro
* Major software platforms
#+ATTR_HTML: :height 500px
[[../img/qgis-01.png]]
* Major software platforms
#+ATTR_HTML: :height 500px
[[../img/grass-01.png]]
* Major software platforms
#+attr_reveal: :frag (appear)
- QGIS
- GRASS
- SAGA GIS
- Whitebox GAT
* Programming frameworks 
#+attr_reveal: :frag (appear)
- The R Project for Statistical Computing
- Python
* Log on to the computer and navigate to the Q:\ drive
#+REVEAL_HTML: <div class="no-write">
#+attr_reveal: :frag (appear)
- Copy the folders from the =Q:\LAB= folder to your personal folder
#+REVEAL_HTML: </div>
