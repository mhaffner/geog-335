import arcpy

arcpy.env.workspace = "Q:/StudentCoursework/Haffnerm/GEOG.335.002.2201/HAFFNERM/data"

# variable for shapefile
in_shp = "C:/Users/haffnerm/Downloads/co_geo.shp"

# declare a variable for the geodatabase
out_gdb = "Q:/StudentCoursework/Haffnerm/GEOG.335.002.2201/HAFFNERM/data/tests.gdb"

# copy features
arcpy.FeatureClassToGeodatabase_conversion(in_shp, out_gdb)

